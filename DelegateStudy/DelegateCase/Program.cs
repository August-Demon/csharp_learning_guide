﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateCase
{
    class Program
    {
        static void Main(string[] args)
        {
            ToolMan toolMan = new ToolMan("小明");

            LazyMan lazyMan1 = new LazyMan("张三");
            LazyMan lazyMan2 = new LazyMan("李四");
            LazyMan lazyMan3 = new LazyMan("王五");

            toolMan.downStairList += lazyMan1.TakeFood;
            toolMan.downStairList += lazyMan2.TakePackage;
            toolMan.downStairList += lazyMan3.TakeFood;

            // 王五变勤快后经常自己取拿东西
            toolMan.downStairList -= lazyMan3.TakeFood;
            // 委托存在的问题 通过=号添加方法会覆盖前面的方法
            toolMan.downStairList = lazyMan3.TakeFood;
            toolMan.TakeStair();
            // 事件没有触发 但是我可以直接触发委托的调用
            toolMan.downStairList();

            Console.ReadKey();

        }
    }
}
