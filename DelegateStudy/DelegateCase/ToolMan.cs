﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateCase
{
    delegate void DownStairDelegate();
    class ToolMan
    {
        public string Name { get; private set; }

        public ToolMan(string name)
        {
            Name = name;
        }
        //public DownStairDelegate downStairList { get; set; }

        public event DownStairDelegate downStairList = null;

        public void TakeStair()
        {
            Console.WriteLine("工具人" + Name + "下楼了");
            if (downStairList != null)
            {
                downStairList();
            }
        }
    }
}
