﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortForDelegate
{
    class Employee
    {
        public Employee(string name, int salary)
        {
            Name = name;
            Salary = salary;
        }

        public string Name { get; private set; }

        public int Salary { get; private set; }

        public static bool Compare(Employee e1, Employee e2)
        {
           return e1.Salary > e2.Salary;
        }
        
    }
}



