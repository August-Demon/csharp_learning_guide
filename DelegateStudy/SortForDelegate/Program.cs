﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortForDelegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] employees = {
                
                new Employee("aa", 1000),
                new Employee("bb", 3000),
                new Employee("xx", 4000),
                new Employee("dd", 9000),
                new Employee("tt", 6000),
                new Employee("rr", 4000),
            };
            Sort<Employee>(employees, Employee.Compare);
            foreach (Employee item in employees)
            {
                Console.WriteLine(item.Name + " " + item.Salary + " ");
            }


            int[] listNum = { 1, 22, 33, 44, 99, 55 };
            Sort(listNum);
            foreach (int item in listNum)
            {
                Console.Write(item + "  ");
            }
            Console.ReadKey();
        }
        /// <summary>
        /// 只能对int类型进行冒泡排序
        /// </summary>
        /// <param name="sortArray"></param>
        static void Sort(int[] sortArray)
        {
            bool swapped = true;
            do
            {
                swapped = false;
                for (int i = 0; i < sortArray.Length - 1; i++)
                {
                    if (sortArray[i] > sortArray[i + 1])
                    {
                        int temp = sortArray[i];
                        sortArray[i] = sortArray[i + 1];
                        sortArray[i + 1] = temp;
                        swapped = true;
                    }
                }

            } while (swapped);
        }

        /// <summary>
        /// 可以对任意类型进行排序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="compare"></param>
        static void Sort<T>(T[] data , Func<T,T,bool> compare)
        {
            bool swapped = true;
            do
            {
                swapped = false;
                for (int i = 0; i < data.Length - 1; i++)
                {
                    if (compare(data[i],data[i+1]))
                    {
                        T temp = data[i];
                        data[i] = data[i + 1];
                        data[i + 1] = temp;
                        swapped = true;
                    }
                }

            } while (swapped);
        }
    }
}
