﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateDemo
{
    class Program
    {
        // 利用委托的特性 把委托类型声明成一个数组，把相同的方法签名的方法当作数组的元素
        // 把委托当作参数传递
        delegate double Opdelegate(double value);
        static void Main(string[] args)
        {
            Opdelegate[] OpLists = { MathOp.MultiplayBytwo, MathOp.Square };

            foreach (Opdelegate item in OpLists)
            {
                Console.WriteLine(item(3));
            }
            ProcessAndDisplayRes(MathOp.Square, 4); 
            Console.ReadKey();
        }

        static void ProcessAndDisplayRes(Opdelegate opdelegateMethod , double value)
        {
            Console.WriteLine(opdelegateMethod(value));
        }
    }
}
